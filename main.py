filepath = 'data.txt'
with open(filepath, 'r') as f:
    data = f.readlines()

i_size = int(data[0].strip('\n').split('x')[0])
j_size = int(data[0].strip('\n').split('x')[1])
table = [i.rstrip('\n').replace(' ','') for i in data[1:i_size+1]]
words = [i.rstrip('\n') for i in data[i_size+1:]]

def left(table, word, i, j, i_current, j_current):
    #Test if left [i][j-1] exists in the table
    if j-1 in range(j_size):
        # Check word second letter in relative direction
        if table[i][j-1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if j_current - n in range(j_size):
                    found_letters.append(table[i_current][j_current - n])
                found_word = ''.join(found_letters)
                # return word if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current}:{j_current-n}"


def right(table, word, i, j, i_current, j_current):
    #Test if right [i][j+1] exists in the table
    if j+1 in range(j_size):
        # Check word second letter in relative direction
        if table[i][j+1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if j_current + n in range(j_size):
                    found_letters.append(table[i_current][j_current + n])
                found_word = ''.join(found_letters)
                # print results if match found and break
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current}:{j_current + n}"


def up(table, word, i, j, i_current, j_current):
    #Test if up [i-1][j] exists in the table
    if i-1 in range(i_size):
        # Check word second letter in relative direction
        if table[i-1][j] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current - n in range(i_size):
                    found_letters.append(table[i_current - n][j_current])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current}:{j_current + n}"


def down(table, word, i, j, i_current, j_current):
    #Test down [i+1][j] exists in the table
    if i+1 in range(i_size):
        # Check word second letter in relative direction
        if table[i+1][j] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current + n in range(i_size):
                    found_letters.append(table[i_current + n][j_current])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current + n}:{j_current}"


def up_left(table, word, i, j, i_current, j_current):
    #Test if up left [i-1][j-1] exists in the table
    if i-1 in range(i_size) and j-1 in range(j_size):
        # Check word second letter in relative direction
        if table[i-1][j-1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current - n in range(i_size) and j_current - n in range(j_size):
                    found_letters.append(table[i_current - n][j_current - n])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current - n}:{j_current - n}"

def up_right(table, word, i, j, i_current, j_current):
    #Test if up right [i-1][j+1] exists in the table
    if i-1 in range(i_size) and j+1 in range(j_size):
        # Check word second letter in relative direction
        if table[i-1][j+1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current - n in range(i_size) and j_current + n in range(j_size):
                    found_letters.append(table[i_current - n][j_current + n])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current - n}:{j_current + n}"


def down_left(table, word, i, j, i_current, j_current):
    #Test if down left [i+1][j-1] exists in the table
    if i-1 in range(i_size) and j+1 in range(j_size):
        # Check word second letter in relative direction
        if table[i-1][j+1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current - n in range(i_size) and j_current + n in range(j_size):
                    found_letters.append(table[i_current - n][j_current + n])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current - n}:{j_current + n}"

def down_right(table, word, i, j, i_current, j_current):
    #Test if down right [i+1][j+1] exists in the table
    if i+1 in range(i_size) and j+1 in range(j_size):
        # Check word second letter in relative direction
        if table[i+1][j+1] == word[1]:
            found_letters = []
            # check remaining  length of word
            for n in range(len(word)):
                if i_current + n in range(i_size) and j_current + n in range(j_size):
                    found_letters.append(table[i_current + n][j_current + n])
                found_word = ''.join(found_letters)
                # print results if match found
                if found_word == word:
                    return f"{found_word} {i}:{j} {i_current + n}:{j_current + n}"

for i in range(i_size):
    for j in range(j_size):
        for word in words:
            # Find first letter of word in the table
            if word[0] == table[i][j]:
                i_current = i
                j_current = j
                left(table, word, i, j, i_current, j_current)
                right(table, word, i, j, i_current, j_current)
                up(table, word, i, j, i_current, j_current)
                down(table, word, i, j, i_current, j_current)
                up_left(table, word, i, j, i_current, j_current)
                up_right(table, word, i, j, i_current, j_current)
                down_left(table, word, i, j, i_current, j_current)
                down_right(table, word, i, j, i_current, j_current)
